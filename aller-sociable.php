<?php
/**
 *  Plugin Name: Aller Sociable
 *  Description: Magically ads social media buttons to content on pages and single posts.
 *  Version: 0.1a
 *  Author: Johannes Henrysson <johannes.henrysson@aller.se>, Aller Media AB
 *
 *  @package Wordpress 3
 *  @subpackage Aller Sociable
 */

define('ALLER_SOCIABLE_PATH', plugin_dir_path(__FILE__));
define('ALLER_SOCIABLE_URL', plugins_url('', __FILE__));

require_once(dirname(__FILE__) . "/aller-sociable.class.php");
require_once(dirname(__FILE__) . "/aller-sociable-admin.class.php");
require_once(dirname(__FILE__) . "/aller-sociable-external-functions.php");

/**
 * Function to make it easier to talk to aller sociable.
 *
 * @param string $output
 *   Mmm, dunno what this is?
 * @param bool $print
 *   Should we print or not? Defaults to TRUE.
 * @param bool $is_page
 *   If you are trying to use aller sociable on a page, you have to set this to TRUE.
 */
if (!function_exists('print_aller_sociable')) {
  function print_aller_sociable($output = '', $print = TRUE, $is_page = FALSE) {
    global $aller_sociable;
    
    if (!is_object($aller_sociable)) {
      return;
    }
    
    $string = $aller_sociable->render_aller_sociable($output, FALSE, $is_page);
    if ($print) {
      print $string;
    } else {
      return $string;
    }
  }
}

// Normal stuff
$aller_sociable = new AllerSociable();
$aller_sociable->init();

// Admin stuff!
$aller_sociable_admin = new AllerSociableAdmin();
add_action('admin_menu', array($aller_sociable_admin, 'add_admin_page'));