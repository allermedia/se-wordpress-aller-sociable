(function($) {
    /**
     * Aller Sociable Counter
     * Fetches and displays reactings for Facebook and Twitter.
     * Could easily be implemented with other social plattforms aswell.
     * 
     * Todo:
     *  Finish implementing Pinterest
     *  
     * @type _L1.allerSociableCounter
     */
    function allerSociableCounter() {
        this.apiUrl = {
            
        }
        this.socialElements = {};
        this.socialUrls = {};
        $(function() {
            this.init();
        }.bind(this));
        var events = new Array(
                'allerSociableCounter:initialised',
                'allerSociableCounter:activeSocialSet',
                'allerSociableCounter:reactionFetched'
                );
        events.forEach(function(event) {
            document.addEventListener(event, this.eventHandler.bind(this));
        }.bind(this));
    }

    /**
     * Handles all events for allerSociableCounter
     * 
     * @param Object e
     * @returns void
     */
    allerSociableCounter.prototype.eventHandler = function(e) {
        switch (e.type) {
            case 'allerSociableCounter:initialised':
                this.setActiveSocial();
                break;
            case 'allerSociableCounter:activeSocialSet':
                this.initCounters(e.detail.socialElements);
                break;
            case 'allerSociableCounter:reactionFetched':
                this.add(e.detail);
                break;
        }
        return;
    };

    /**
     * Inititialises allerSociableCounter
     * 
     * Fires event allerSociableCounter:initialised
     * indicating that DOM is ready
     * 
     * @returns void
     */
    allerSociableCounter.prototype.init = function() {
        var event = new CustomEvent(
                'allerSociableCounter:initialised',
                {detail: {
                        'this': this
                    }
                }
        );
        document.dispatchEvent(event);
        return;
    };

    /**
     * Finds all social buttons
     * 
     * Fires event allerSociableCounter:activeSocialSet
     * when all social bottons are found.
     * 
     * Uses params set in global allerSociableAjax.settings from WP
     * 
     * @returns void
     */
    allerSociableCounter.prototype.setActiveSocial = function() {

        if (allerSociableAjax.settings.facebook) {
            this.socialElements.facebook = $('.aller-sociable .fa-facebook');
            $('.aller-sociable .fa-facebook').addClass('reactions');
        }
        if (allerSociableAjax.settings.twitter) {
            this.socialElements.twitter = $('.aller-sociable .fa-twitter');
            $('.aller-sociable .fa-twitter').addClass('reactions');
        }

        var event = new CustomEvent(
                'allerSociableCounter:activeSocialSet',
                {detail: {
                        'socialElements': this.socialElements
                    }
                }
        );
        document.dispatchEvent(event);

        return;
    };

    /**
     * Initialises the featch reaction call for each button
     * 
     * @see fetchReactions
     * @param Object socialElements
     * @returns void
     */
    allerSociableCounter.prototype.initCounters = function(socialElements) {

        for (var handler in socialElements) {
            if (socialElements.hasOwnProperty(handler)) {
                if (socialElements[handler].length > 0) {
                    if(typeof $(socialElements[handler]).data('count') === 'number') {
                        this.fetchReactions(handler, socialElements[handler]);
                    }
                }
            }
        }
        return;
    };

    /**
     * Get the reaction count for each button
     * 
     * Does an ajax call to backend that does the accual API calls
     * Dispatches event allerSociableCounter:reactionFetched with all data
     * 
     * @param String handler
     * @param Array elements
     * @returns void
     */
    allerSociableCounter.prototype.fetchReactions = function(handler, elements) {

        $(elements).each(function(index) {
            var element = elements[index];
            var shareUrl = $(element).data("shareurl");
            var count = $(element).data("count");
            var handler = $(element).data("handler");
            var transient = $(element).data("transient");
            var key = $(element).data("key");
            var displayCount = $(element).data("displaycount");
            this.socialUrls[shareUrl] = 0;

            var event = new CustomEvent(
                    'allerSociableCounter:reactionFetched',
                    {detail: {
                            'handler': handler,
                            'shareUrl': shareUrl,
                            'count': count,
                            'displayCount': displayCount,
                            'key': key,
                            'transient': transient,
                            'element': element
                        }
                    }
            );

            document.dispatchEvent(event);

        }.bind(this));
        return;
    };

    /**
     * Adds the reaction number to each button
     * 
     * Adds the template containing the number of reactions.
     * Is triggerd from eventHander with the event 
     * allerSociableCounter:reactionFetched
     * 
     * @param Object obj
     * @returns void
     */
    allerSociableCounter.prototype.add = function(obj) {

        var template = this.getCounterTemplate();

        template = template.replace('{placeholderClass}', obj.handler + '-counter');
        template = template.replace('{placeholderCounter}', obj.displayCount);
        template = template.replace('{placeholderDataCounter}', obj.count);
        template = template.replace('{placeholderDataShareUrl}', obj.shareUrl);
        template = template.replace('{placeholderDataHandler}', obj.handler);
        template = template.replace('{placeholderDataTransient}', obj.transient);

        var wrapper = $(obj.element).find('.counterWrapper');

        if (wrapper.length == 0) {
            $(obj.element).append(template);
        } else {
            $(wrapper).html(template);
        }

        return;
    };

    /**
     * Fetches the template structure
     * 
     * The template is saved in a global variable and can be set with a 
     * WP filter called allerSociableCounterTemplateSource or
     * allerSociableCounterTemplatePath
     * 
     * @returns String
     */
    allerSociableCounter.prototype.getCounterTemplate = function() {
        return allerSociableCounterTemplate;
    };

    var allerSociableCounter = new allerSociableCounter();
})(jQuery);

/**
 * Polyfill CustomEvent() for Internet Explorer 9 and 10
 *
 * @link https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent
 */
(function()
{
    function CustomEvent(event, params)
    {
        params = params || {bubbles: false, cancelable: false, detail: undefined};
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }
    ;
    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
})();