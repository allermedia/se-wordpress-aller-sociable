<div class="wrap">
  <div id="icon-options-general" class="icon32"><br /></div>
  <h2>Aller Sociable</h2>
  
  <?php if (isset($message)) : ?>
    <div id="setting-error-settings_updated" class="updated settings-error">
      <p><strong><?php print $message; ?></strong></p>
    </div>
  <?php endif; ?>
  
  <form action="" method="POST">
    <?php wp_nonce_field('aller-sociable'); ?>
    
    <table class="form-table">
      <tr valign="top">
        <th scope="row">
          <label for="hook_content"><?php _e('Hook to content'); ?></label>
        </th>
        <td>
          <input type="checkbox" name="hook_content" id="hook_content" <?php if ($settings['hook_content'] == 1 || $settings['hook_content'] === FALSE) : ?>checked<?php endif; ?>/>
          <span class="description"><?php _e('Hook sociable buttons to content. If not, inlcude print_aller_sociable(); on desired place in theme.'); ?></span>
        </td>
      </tr>
      
      <tr valign="top">
        <th scope="row">
          <label for="all_pages"><?php _e('All pages'); ?></label>
        </th>
        <td>
          <input type="checkbox" name="all_pages" id="all_pages" <?php if ($settings['all_pages'] == 1) : ?>checked<?php endif; ?> />
          <span class="description"><?php _e('Show sociable buttons on all pages, including flow etc.'); ?></span>
        </td>
      </tr>
      <tr valign="top">
        <th scope="row">
          <?php _e('Active buttons'); ?>
        </th>
        <td>
        <span class="description"><?php _e('Choose what kind of icon you want'); ?></span><br>
          <label title="Facebook <?php _e('button'); ?>">
            <input type="text" name="facebook" id="facebook" class="small-text" value="<?php print $facebook; ?>" />
            <span>Facebook</span>
            <br>
            <input type="radio" name="facebook_icon" id="facebook_icon" class="icon" value="icon" <?php if($facebook_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="facebook_icon" id="facebook_icon" class="icon" value="default" <?php if($facebook_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
            <br>
            <input type="checkbox" name="facebook_counter" class="counter" value="1" <?php if($facebook_counter == 1){ echo 'checked'; } ?> >
            <span>Use reactions counter</span>
          </label><br/>
          <label title="Twitter <?php _e('button'); ?>">
            <input type="text" name="twitter" id="twitter" class="small-text" value="<?php print $twitter; ?>" />
            <span>Twitter</span>
            <br>
            <input type="radio" name="twitter_icon" id="twitter_icon" class="icon" value="icon" <?php if($twitter_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="twitter_icon" id="twitter_icon" class="icon" value="default" <?php if($twitter_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
            <br>
            <input type="checkbox" name="twitter_counter" class="counter" value="1" <?php if($twitter_counter == 1){ echo 'checked'; } ?> >
            <span>Use reactions counter</span>
          </label><br/>
          <label title="Google+ <?php _e('button'); ?>">
            <input type="text" name="google" id="google" class="small-text" value="<?php print $google; ?>" />
            <span>Google+</span>
            <br>
            <input type="radio" name="google_icon" id="google_icon" class="icon" value="icon" <?php if($google_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="google_icon" id="google_icon" class="icon" value="default" <?php if($google_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
          </label><br/>
          <label title="Print <?php _e('button'); ?>">
            <input type="text" name="print" id="print" class="small-text" value="<?php print $print; ?>" />
            <span>Print</span>
            <br>
            <input type="radio" name="printfriendly_icon" id="print_icon" class="icon" value="icon" <?php if($printfriendly_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="printfriendly_icon" id="print_icon" class="icon" value="default" <?php if($printfriendly_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
          </label><br />
          <label title="Mail <?php _e('button'); ?>">
            <input type="text" name="mail" id="mail" class="small-text" value="<?php print $mail; ?>" />
            <span>Mail</span>
            <br>
            <input type="radio" name="mail_icon" id="mail_icon" class="icon" value="icon" <?php if($mail_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="mail_icon" id="mail_icon" class="icon" value="default" <?php if($mail_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
          </label><br />
          <label title="Pinterest <?php _e('button'); ?>">
            <input type="text" name="pinterest" id="pinterest" class="small-text" value="<?php print $pinterest; ?>" />
            <span>Pinterest</span>
            <br>
            <input type="radio" name="pinterest_icon" id="pinterest_icon" class="icon" value="icon" <?php if($pinterest_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="pinterest_icon" id="pinterest_icon" class="icon" value="default" <?php if($pinterest_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
          </label><br />
          <label title="Tumblr <?php _e('button'); ?>">
            <input type="text" name="tumblr" id="tumblr" class="small-text" value="<?php print $tumblr; ?>" />
            <span>Tumblr</span>
            <br>
            <input type="radio" name="tumblr_icon" id="tumblr_icon" class="icon" value="icon" <?php if($tumblr_icon == 'icon'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off">
            <span>Font-awesome ikon</span>
            <br>
            <input type="radio" name="tumblr_icon" id="tumblr_icon" class="icon" value="default" <?php if($tumblr_icon == 'default'){ echo 'checked'; } else{ echo ''; } ?> autocomplete="off"/>
            <span>Standard bild</span>
          </label><br />
          <span class="description"><?php _e('Priority for buttons. 0 = not visible. Lower numbers comes first.'); ?></span>
        </td>
      </tr>
      
      <tr valign="top">
        <th scope="row">
          <?php _e('Custom content'); ?>
        </th>
        
        <td>
          <label title="Print <?php _e('image'); ?>">
            <span>Print</span>
            <textarea name="printfriendly_image" id="printfriendly_image" class="large-text"><?php if (isset($settings['image']['printfriendly_image'])) print $settings['image']['printfriendly_image']; ?></textarea>            
          </label><br />
          <label title="Mail <?php _e('image'); ?>">
            <span>Mail</span>
            <textarea name="mail_image" id="mail_image" class="large-text"><?php if (isset($settings['image']['mail_image'])) print $settings['image']['mail_image']; ?></textarea>
          </label><br />
          <span class="description"><?php _e('Write complete HTML for what you want inside buttons.'); ?></span>
        </td>
      </tr>
      
      
      <tr valign="top">
        <th scope="row">
          <label for="pre"><?php _e('Pre buttons'); ?></label>
        </th>
        <td>
          <textarea name="pre" id="pre" rows="8" cols="35" class="large-text code"><?php print $settings['pre']; ?></textarea>
        </td>
      </tr>
      
      <tr valign="top">
        <th scope="row">
          <label for="post"><?php _e('Post buttons'); ?></label>
        </th>
        <td>
          <textarea name="post" id="post" rows="8" cols="35" class="large-text code"><?php print $settings['post']; ?></textarea>
        </td>
      </tr>
    </table>
    
    <p class="submit">
      <input type="submit" name="submit" id="submit" class="button-primary" value="<?php _e('Save Changes'); ?>" />
    </p>
  </form>
</div>
