<?php
/**
 * Functions for external use, when you doing something that is just to fancy
 * to use standard functionality and functions.
 *
 * NOTE! If you just want to add all buttons according to settings in admin, use
 * function print_aller_sociable(), which is found in aller-sociable.php.
 */

/**
 * Render custom print button.
 *
 * @param string $content
 *   Easy, define all content that you wish between a tags, or leave it for default
 *   value.
 * @param boolean $echo
 *   Return as string or print?
 * @return string|nothing
 *   Returns result as string, depending on params above.
 */
function as_custom_print($content = '', $echo = TRUE) {
  global $aller_sociable;
  
  $string = $aller_sociable->add_printfriendly(array('printfriendly' => $content), FALSE);
  
  if ($echo) {
    print $string;
  } else {
    return $string;
  }
}

/**
 * Render custom email link/button.
 *
 * @param string $content
 *   Quite easy, define all content within a tags, or leave it for default value.
 * @param boolean $echo
 *   Return as string or print?
 * @return string|nothing
 *   Returns result as string, depending on params above.
 */
function as_custom_email($content = '', $echo = TRUE) {
  global $aller_sociable;
  
  $string = $aller_sociable->add_mail(array('mail' => $content));
  
  if ($echo) {
    print $string;
  } else {
    return $string;
  }
}

/**
 * Render custom twitter look. Which is actually not that custom.
 *
 * @param boolean $echo
 * @return string|nothing
 */
function as_custom_twitter($echo = TRUE) {
  global $aller_sociable;
  
  $string = $aller_sociable->add_twitter();
  
  if ($echo) {
    print $string;
  } else {
    return $string;
  }
}

/**
 * Render custom facebook look. Which is actually not that custom.
 *
 * @param boolean $echo
 * @return string|nothing
 */
function as_custom_facebook($echo = TRUE) {
  global $aller_sociable;
  
  $string = $aller_sociable->add_facebook();
  
  if ($echo) {
    print $string;
  } else {
    return $string;
  }
}

/**
 * Render custom pinterest look. Which is actually not that custom.
 *
 * @param boolean $echo
 * @return string|nothing
 */
function as_custom_pinterest($echo = TRUE) {
  global $aller_sociable;
  
  $string = $aller_sociable->add_pinterest();
  
  if ($echo) {
    print $string;
  } else {
    return $string;
  }
}