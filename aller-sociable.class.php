<?php
require_once(dirname(__FILE__) . "/aller-sociable-admin.class.php");

/**
 *  Class to actually add buttons etc to Aller Sociable.
 *
 *  @author Johannes Henrysson <johannes.henrysson@aller.se>, Aller Media AB
 *  
 *  @package Wordpress 3
 *  @subpackage Aller Sociable
 */
class AllerSociable extends AllerSociableAdmin
{  

  var $request_url = "";
  
  /**
   *  Stuff that we need, or probably need...
   */
  function __construct() {
    
    // Init buttons
    add_action('wp_footer', array($this, 'add_google_script'));
    add_action('wp_footer', array($this, 'add_printfriendly_script'));
    add_action('wp_footer', array($this, 'add_pinterest_script'));
    add_action('wp_footer', array($this, 'add_tumblr_script'));
    add_action('wp_enqueue_scripts', array($this, 'addSocialCounter'));

    if (is_admin()) {
            add_action('wp_ajax_get_aller_sociable', array($this, 'get_aller_sociable_callback'));
            add_action('wp_ajax_nopriv_get_aller_sociable', array($this, 'get_aller_sociable_callback'));
            add_action('wp_ajax_get_reactions', array($this, 'getReaction'));
            add_action('wp_ajax_nopriv_get_reactions', array($this, 'getReaction'));
        }
  }
  
  /**
   *  Init action!
   *
   *  Check with database what to do, and do it!
   */
  function init() {
    $hook = get_option('aller-sociable-hook');
    // Fix for Wordpress
    if ($hook == 1 || $hook === FALSE)
      add_filter('the_content', array($this, 'render_aller_sociable'), 100);
  }
  
  /**
   *  Render Aller Sociable section.
   *
   *  @param string $output
   *  @param boolean $print
   *  @param boolean $is_page
   *    If you are trying to use this on a page, you have to set this to yes.
   *  @return string
   *    New outout, if single.
   */
  function render_aller_sociable($output = '', $print = FALSE, $is_page = FALSE) {
    if (is_feed()) {
      return $output;
    }
    
    if (get_option('aller-sociable-all') != 1 && !is_single() && !$is_page) {
      return $output;
    }
    
    $settings = $this->get_settings();
    
    if(get_permalink())
      $this->request_url = get_permalink();

    $pre = !empty($settings['pre']) ? $settings['pre'] : '<div class="aller-sociable" style="width:100%;height:32px;line-height:24px;clear:both;">';
    $post = !empty($settings['post']) ? $settings['post'] : '</div>';
    
    $output .= $pre;
    if (!empty($settings['prio'])) {
      uasort($settings['prio'], array($this, 'compare_array'));
      foreach($settings['prio'] as $key => $value) {
        if ($value == 0)
          continue;
        // Backwards compability...
        if ($key == 'print')
          $key = 'printfriendly';
        if (isset($settings['image'][$key]))
          $output .= $this->add_button($key, $settings['image'][$key]);
        else
          $output .= $this->add_button($key);
      }
    } else {
      $output .= $this->add_button('printfriendly');
      $output .= $this->add_button('facebook');
      $output .= $this->add_button('twitter');
      $output .= $this->add_button('google');
      $output .= $this->add_button('mail');
      $output .= $this->add_button('pinterest');
      $output .= $this->add_button('tumblr');
    }
    $output .= $post;
    
    if ($print)
      print $output;
    else
      return $output;
  }
  
  /*
  * Get a sociable bar ajax
  */
  function get_aller_sociable_callback() {
    $this->request_url = $_POST['url'];
    $this->render_aller_sociable('', true, true);
    die();
  }
  /**
   *  Callback for uasort, to sort an array after value, while keeping key.
   *
   *  @param mixed $a
   *  @param mixed $b
   *  @return int
   */
  function compare_array($a, $b) {
    if ($a == $b) {
      return 0;
    }
    return ($a < $b) ? -1 : 1;
  }
  
  /**
   *  Add button of any kind.
   *
   *  @param string $type
   *    What kind do we want to add, for instance 'facebook' or 'google'.
   *  @return mixed
   *    If found, return value from function.
   */
  public function add_button($type) {
    $function = "add_{$type}";
    if (method_exists($this, $function))
      return $this->$function();
    else
      return;
  }

 /**
  * Get sociable icon image
  * @param string medium
  *   what kinda medium is it, facebook, pinterest etc.
  * @return string
  *   Choosen icon
  */
  public function get_sociable_image($medium){

    if(isset($medium) && !empty($medium)){

    $settings = $this->get_settings();
    $icon = $settings['image'][$medium . '_icon'];

    $images = array(
      'default' => '',
      'icon'    => '<i class="fa fa-' . $medium . '"></i>',
      'upload'  => $logo[$medium]['upload']
      );
    }
    return $images[$icon];

    //assets.pinterest.com/images/PinExt.png
  }

  /**
   *  Adds facebook button, with count box.
   *
   *  @return string
   *    Complete HTML button.
   */
  function add_facebook() {

    $settings = $this->get_settings();

    if($settings['image']['facebook_icon'] == 'icon'){
      $reactions = $this->getReaction('facebook', urlencode($this->request_url));
      $facebook_icon = '<a title="Dela på Facebook" style="cursor:pointer;" onclick="window.open(\'http://www.facebook.com/sharer.php?u=' . urlencode($this->request_url) . '\', \'Facebook\', \'toolbar=no,width=550,height=550\'); return false;" rel="nofollow" style="padding:0;">
      <i class="fade-effect fa fa-facebook" data-transient="'.$reactions['transient'].'" data-key="'.$reactions['key'].'" data-displaycount="'.$reactions['displayCount'].'" data-handler="'.$reactions['handler'].'" data-count="'.$reactions['count'].'" data-shareurl="' . urlencode($this->request_url) . '"><span class="textWrapper"></span><span class="counterWrapper"><span class="loading"></span></span></i></a>';
    }
    else{
      $facebook_icon = '<iframe src="//www.facebook.com/plugins/like.php?href=' . urlencode($this->request_url) .'&amp;locale=sv_SE&amp;layout=button_count&amp;action=like&amp;show_faces=false&amp;share=true&amp;height=21&amp;" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width: 130px; height:21px;" allowTransparency="true"></iframe>';
    }
    return $facebook_icon;
  }
  
  /**
   *  Get Facebook total count, for count box.
   *
   *  @return string
   *    total_count from Facebook API.
   */
  function _fb_count() {
    $api_url = 'http://api.facebook.com/restserver.php?method=links.getStats&urls=' . $this->request_url;
    
    try {
      $xml = new SimpleXMLElement($api_url, 0, TRUE);
    } catch (Exception $e) {
      return '?';
    }
    
    return $xml->link_stat->total_count;
  }
  
  /**
   *  Adds google button.
   *
   *  @return string
   *    Complete HTML button.
   */
  function add_google() {
    $settings = $this->get_settings();
    if($settings['image']['google_icon'] == 'icon'){
      return '<a href="https://plus.google.com/share?url=' . $this->request_url . '" ><i class="fade-effect fa fa-google-plus"></i></a>';
    }
    else{
      return '<g:plusone size="medium" href="' . $this->request_url . '"></g:plusone>';
    }
  }
  
  /**
   *  Adds necessary javascript to footer. Needs to be public for ugly add_action
   *  to work ;)
   */
  public function add_google_script() {
    include(dirname(__FILE__) . "/includes/google_script.html");
  }
  
  /**
   *  Adds twitter button
   *
   *  @return string
   *    Complete HTML button.
   */
  function add_twitter() {

    $settings = $this->get_settings();

      if($settings['image']['twitter_icon'] == 'icon'){
        $reactions = $this->getReaction('twitter', urlencode($this->request_url));
        $twitter_icon = '<script type="text/javascript" src="//platform.twitter.com/widgets.js"></script>
        <a href="https://twitter.com/intent/tweet?text=' . urlencode(get_the_title()) . '&url=' . $this->request_url . '&lang=sv"><i class="fade-effect fa fa-twitter" data-transient="'.$reactions['transient'].'" data-key="'.$reactions['key'].'" data-displaycount="'.$reactions['displayCount'].'" data-handler="'.$reactions['handler'].'" data-count="'.$reactions['count'].'" data-shareurl="' . urlencode($this->request_url) . '"><span class="textWrapper"></span><span class="counterWrapper"><span class="loading"></span></span></i></a>';
      }
      else{
        $twitter_icon = '<iframe allowtransparency="true" frameborder="0" scrolling="no"
        src="//platform.twitter.com/widgets/tweet_button.html?url=' . $this->request_url . '&text=' . get_the_title() . '&lang=sv"
        style="width:92px; height:20px;"></iframe>';
      }
      return $twitter_icon;
  }
  
  /**
   *  Add button for printfriendly.
   *
   *  @param array $image
   *    Custom image/content.
   *  @param boolean $styling
   *    Add inline styling or not.
   *  @return string
   *    Complete HTML button.
   */
  function add_printfriendly($image = NULL, $styling = TRUE) {
    if (!isset($image))
      $image = get_option('aller-sociable-image');
      $settings = $this->get_settings();
      $image['printfriendly_image'] = !empty($image['printfriendly_image']) ? stripslashes($image['printfriendly_image']) :
        '<img style="margin:0;" src="http://cdn.printfriendly.com/button-print-gry20.png" alt="Print Friendly and PDF" />';
      
      $styling = ($styling) ? 'style="padding:0 10px 0 0; text-decoration:none;"' : '';
      if(isset($settings['image']['printfriendly_icon']) && $settings['image']['printfriendly_icon'] == 'icon'){
        return sprintf('<a href="www.printfriendly.com/print/v2?url=%s" %s class="printfriendly" onclick="window.print(); return false;" title="Skriv ut" rel="nofollow">%s</a>',
        urlencode($this->request_url), $styling, '<i class="fade-effect fa fa-print"></i>');
      }
      else{
        return sprintf(
            '<a href="www.printfriendly.com/print/v2?url=%s" %s class="printfriendly" onclick="window.print(); return false;" title="Skriv ut" rel="nofollow">%s</a>',
            urlencode($this->request_url), $styling, $image['printfriendly_image']
        );
    }
  }
  
  /**
   *  Add necessary javascript for printfriendly.
   */
  public function add_printfriendly_script() {
    include(dirname(__FILE__) . "/includes/printfriendly_script.html");
  }
  
  /**
   *  Add mail link.
   *
   *  @param array $image
   *    Custom image/content variable.
   *  @return string
   *    Complete button as HTML
   */ 
  function add_mail($image = NULL) {

    if (!isset($image))
      $image = get_option('aller-sociable-image');
      $image['mail_image'] = !empty($image['mail_image']) ? stripslashes($image['mail_image']) :
      '<span style="font-weight:bold; line-height:20px;">Maila:</span> <img src="' . plugins_url('images/mail.png', __FILE__) . '" alt="Tipsa en vän!" />';

      $settings = $this->get_settings();

      if($settings['image']['mail_icon'] == 'icon'){
        return '<a href="mailto:?subject=' . strip_tags(get_the_title()) . '&body=' . urlencode($this->request_url) . '" title="Tipsa en vän!"><i class="fade-effect fa fa-envelope"></i></a>'; 
      }
      else{
        return '<a href="mailto:?subject=' . strip_tags(get_the_title()) . '&body=' . urlencode($this->request_url) . '" title="Tipsa en vän!">' . $image['mail_image'] . '</a>';
      }
  }
  
      /**
   *  Adds pinterest button, with count box.
   *
   *  @return string
   *    Complete HTML button.
   */
  function add_pinterest() {
    $first_img = '';
    if (is_attachment()) {
      global $wp_query;
      $first_img = wp_get_attachment_image_src($wp_query->post->ID, "article-image-large");
      (string)$first_img = $first_img[0];
    } else {
      $content = get_the_content();
      $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $content, $matches);
      $descoutput = preg_match_all('/<img.+alt=[\'"]([^\'"]+)[\'"].*>/i', $content, $descmatches); //Sooo lazy. Let's just copy this regex and modify it slightly
      if(isset($matches[1][0])){
        $first_img = $matches[1][0];
      }
      $description = "";
      if (isset($descmatches[1][0])) {
        $description = $descmatches[1][0];
      }
    }
    if(empty($first_img)){ // Just show Pinterest-logo if no image is found - should be harmless :-P
      $first_img = "http://passets-ec.pinterest.com/images/about/logos/Logo.png";
    }
    $settings = $this->get_settings();
    if($settings['image']['pinterest_icon'] == 'icon'){
      return $pinterest = '<a href="http://pinterest.com/pin/create%2Fbutton/?url=' . urlencode($this->request_url) . '&media=' . $first_img  . '&description=' . $description . '" class="pin-it-button" count-layout="horizontal"><i class="fade-effect fa fa-pinterest"></i></a>';
    }
    else{
      return $pinterest = '<a href="http://pinterest.com/pin/create%2Fbutton/?url=' . urlencode($this->request_url) . '&media=' . $first_img  . '&description=' . $description . '" class="pin-it-button" count-layout="horizontal"><img style="margin: 0px;" border="0" src="//assets.pinterest.com/images/pidgets/pin_it_button.png" title="Pin It" /></a>';
    }
  }
  
    /**
   *  Add necessary javascript for pinterest.
   */
  public function add_pinterest_script() {
    echo '<script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>';
    //Old method, delete if still left after new one is tested
    //include(dirname(__FILE__) . "/includes/pinterest_script.html");
  }
      /**
   *  Adds tumblr button.
   *
   *  @return string
   *    Complete HTML button.
   */
  function add_tumblr() {
    $settings = $this->get_settings();
    if($settings['image']['tumblr_icon'] == 'icon'){
      return '<a href="http://www.tumblr.com/share" title="Share on Tumblr"><i class="fade-effect fa fa-tumblr-square"></i></a>';
    }
    else{
      return '<a href="http://www.tumblr.com/share" title="Share on Tumblr" style="display:inline-block; text-indent:-9999px; overflow:hidden; width:61px; height:20px; background:url(\'http://platform.tumblr.com/v1/share_2.png\') top left no-repeat transparent;">Share on Tumblr</a> ';
    }
  }
  
    /**
   *  Add necessary javascript for pinterest.
   */
  public function add_tumblr_script() {
    echo '<script src="http://platform.tumblr.com/v1/share.js"></script>';
  }
  
  /**
   * Adds Social counter needed scripts and params
   */
  public function addSocialCounter()
    {
        // Get settings
        $settings = $this->get_settings();
        $counterSettings = array(
            'twitter' => ($settings['image']['twitter_counter']) ? (int) $settings['image']['twitter_counter'] : 0,
            'facebook' => ($settings['image']['facebook_counter']) ? (int) $settings['image']['facebook_counter'] : 0,
        );
        $activateCounter = in_array(1, $counterSettings);
        
        // We do not need to load any scripts or params
        // if no reaction counters are active
        if ($activateCounter) {
            // Load the script for the social reactions counter
            wp_enqueue_script(
                    'aller-sociable-counter', ALLER_SOCIABLE_URL . '/js/counter.js', array('jquery'), '1.0.0'
            );
            // Make some params avaible to the counter.js script
            wp_localize_script(
                    'aller-sociable-counter', 'allerSociableAjax', array(
                'ajaxUrl' => admin_url('admin-ajax.php'),
                'ALLER_SOCIABLE_PATH' => ALLER_SOCIABLE_PATH,
                'ALLER_SOCIABLE_URL' => ALLER_SOCIABLE_URL,
                'settings' => $counterSettings
                    )
            );
            // Load the template for the social reactions counter
            ob_start();
            include apply_filters('allerSociableCounterTemplatePath', ALLER_SOCIABLE_PATH . '/templates/allerSociableCounterTemplate.php');
            $defaultTemplate = apply_filters('allerSociableCounterTemplateSource', ob_get_clean());
            wp_localize_script(
                    'aller-sociable-counter', 'allerSociableCounterTemplate', $defaultTemplate
            );
        }
    }

    /**
     * Ajax callback function for action get_reactions
     * 
     * Do not get any params in function but requires
     * POST-data (handler, handler, apiUrl) to be sent.
     * 
     * POST-data:
     * apiUrl, the api url we will make an request to
     * shareUrl, the url we want to fetch share counts on
     * handler, the service name we are making the reques to
     * 
     */
    public function getReaction($handler, $shareUrl)
    {
        $apiUrl = array(
            'twitter' => "http://urls.api.twitter.com/1/urls/count.json?url=",
            'facebook' => "http://api.facebook.com/restserver.php?method=links.getStats&format=json&urls=",
            'pintrest' => "http://api.pinterest.com/v1/urls/count.json?callback=receiveCount&url="
        );
        $apiUrl = $apiUrl[$handler];

        $transient = true;
        $key = sha1($handler . $shareUrl);

        // Check if we have a cache first
        if (false === ($count = get_transient($key))) {
            // Get a new json respons
            $respons = json_decode(file_get_contents($apiUrl . $shareUrl));
            $transient = false;

            $count = array(
                'key' => $key,
                'handler' => $handler,
                'shareUrl' => $shareUrl,
            );

            // We need to handle the diffrent responses a little
            // diffrent for each api
            switch ($handler) {
                case 'twitter':
                    $count['count'] = $respons->count;
                    break;
                case 'facebook':
                    $count['count'] = $respons[0]->total_count;
                    break;
            }

            // We do not want to display numbers above 999 without 
            // doing some filtering
            $count['displayCount'] = $count['count'];
            if ($count['displayCount'] > 99999) {
                $count['displayCount'] = round((($count['displayCount'] / 1000) * 10) / 10, 0) . "k";
            }
            else if ($count['displayCount'] > 9999) {
                $count['displayCount'] = round((($count['displayCount'] / 1000) * 10) / 10) . "k";
            }
            else if ($count['displayCount'] > 999) {
                $count['displayCount'] = (round($count['displayCount'] / 1000, 2)) . 'k';
            }
            
            $count['displayCount'] = apply_filters('allerSociableCounterDisplayCount', $count['displayCount'], $count['count']);

            // Cache this for 1-3 hour or whatever the filter says
            set_transient($key, $count, apply_filters('allerSociableCounterCache', rand(3600, 10800)));
        }

        $count['transient'] = $transient;
        
        return $count;
    }
}
