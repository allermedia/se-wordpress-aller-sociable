<?php
/**
 *  Administration area for Aller Sociable.
 *
 *  @package Wordpress 3
 *  @subpackage Aller Sociable
 */
class AllerSociableAdmin
{
  /**
   *  Add administration page to Wordpress admin menus.
   */
  function add_admin_page() {
    add_options_page('Aller Sociable', 'Aller Sociable', 'manage_options',
      'aller-sociable', array($this, 'render_admin_page'));
    // add_action('wp_enqueue_scripts', array($this, 'add_scripts')); avoid this for now, fast and simple...
  }
  
  /**
   *  Add draggable for flashy admin stuff.
   */
  function add_scripts() {
    wp_enqueue_script('jquery-ui-draggable');
  }
  
  /**
   *  Render admin page itself
   */
  function render_admin_page() {
    if (!current_user_can('manage_options'))
      die(_e('Error: Current user has no access.'));
    
    if (isset($_POST['submit']))
      $message = $this->save_settings($_POST);
    
    $settings = $this->get_settings();
    if (is_array($settings['prio'])) {
      extract($settings['prio']);
    }
    if(is_array($settings['image'])){
      extract($settings['image']);
    }

    $facebook = isset($facebook) ? $facebook : 1;
    $google = isset($google) ? $google : 1;
    $twitter = isset($twitter) ? $twitter : 1;
    $print = isset($print) ? $print : 1;
    $mail = isset($mail) ? $mail : 1;
    $pinterest = isset($pinterest) ? $pinterest : 0;
    $tumblr = isset($tumblr) ? $tumblr : 0;

    $facebook_icon = isset($facebook_icon) ? $facebook_icon : 'default';
    $google_icon = isset($google_icon) ? $google_icon : 'default';
    $twitter_icon = isset($twitter_icon) ? $twitter_icon : 'default';
    $printfriendly_icon = isset($printfriendly_icon) ? $printfriendly_icon : 'default';
    $mail_icon = isset($mail_icon) ? $mail_icon : 'default';
    $pinterest_icon = isset($pinterest_icon) ? $pinterest_icon : 'default';
    $tumblr_icon = isset($tumblr_icon) ? $tumblr_icon : 'default';
    
    $facebook_counter = isset($facebook_counter) ? $facebook_counter : 0;
    $twitter_counter = isset($twitter_counter) ? $twitter_counter : 0;

    include(dirname(__FILE__) . "/templates/admin.php");
  }
  
  /**
   *  Save settings to database.
   *
   *  @param array $data
   *  @return string
   *    Message to user.
   */
  function save_settings($data) {    
    if (!wp_verify_nonce($_POST['_wpnonce'], 'aller-sociable'))
      return __('Something went wrong.');
    
    $save = array();
    $keys = array('facebook', 'google', 'twitter', 'print', 'mail', 'pinterest', 'tumblr');
    foreach($keys as $k) {
      $save[$k] = is_numeric($data[$k]) ? $data[$k] : 0;
    }
    $data['hook_content'] = !empty($data['hook_content']) ? 1 : 0;
    $data['all_pages'] = !empty($data['all_pages']) ? 1 : 0;
    
    if (isset($data['mail_image']))
      $data['image']['mail_image'] = $data['mail_image'];
    if (isset($data['printfriendly_image']))
      $data['image']['printfriendly_image'] = $data['printfriendly_image'];
    if (isset($data['mail_icon']))
      $data['image']['mail_icon'] = $data['mail_icon'];
    if (isset($data['printfriendly_icon']))
      $data['image']['printfriendly_icon'] = $data['printfriendly_icon'];
    if (isset($data['facebook_icon']))
      $data['image']['facebook_icon'] = $data['facebook_icon'];
    if (isset($data['google_icon']))
      $data['image']['google_icon'] = $data['google_icon'];
    if (isset($data['twitter_icon']))
      $data['image']['twitter_icon'] = $data['twitter_icon'];
    if (isset($data['pinterest_icon']))
      $data['image']['pinterest_icon'] = $data['pinterest_icon'];
    if (isset($data['tumblr_icon']))
      $data['image']['tumblr_icon'] = $data['tumblr_icon'];
    
    if (isset($data['facebook_counter']))
      $data['image']['facebook_counter'] = $data['facebook_counter'];
    if (isset($data['twitter_counter']))
      $data['image']['twitter_counter'] = $data['twitter_counter'];
    
    // Sweet wonderful wordpress returns false on both fail and if nothing has changed.
    // So impossible to return valid message to user...
    // Liar! WP returns true if option value has changed, false if not or if update failed.
    update_option('aller-sociable-prio', $save);
    update_option('aller-sociable-hook', $data['hook_content']);
    update_option('aller-sociable-all', $data['all_pages']);
    update_option('aller-sociable-pre', $data['pre']);
    update_option('aller-sociable-post', $data['post']);
    update_option('aller-sociable-image', $data['image']);
    
    return __('Settings saved.');
  }
  
  /**
   *  Get settings from database.
   *
   *  @return array
   *    Settings from database.
   */
  function get_settings() {
    $data = array();
    
    $data['prio'] = get_option('aller-sociable-prio');
    $data['hook_content'] = stripslashes(get_option('aller-sociable-hook'));
    $data['all_pages'] = stripslashes(get_option('aller-sociable-all'));
    $data['pre'] = stripslashes(get_option('aller-sociable-pre'));
    $data['post'] = stripslashes(get_option('aller-sociable-post'));
    $data['image'] = get_option('aller-sociable-image');
    if (is_array($data['image'])) {
      foreach($data['image'] as &$d) {
        $d = stripslashes($d);
      }
    }
    
    return $data;
  }
}
